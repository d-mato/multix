(function() {
  chrome.browserAction.onClicked.addListener(function(tab) {
    return chrome.tabs.create({
      url: chrome.extension.getURL('index.html')
    });
  });

  chrome.runtime.onInstalled.addListener(function() {
    return chrome.contextMenus.create({
      id: "dl",
      title: "mutlix",
      type: "normal",
      contexts: ["link"]
    });
  });

  chrome.contextMenus.onClicked.addListener(function(info, tab) {
    if (info.menuItemId === "dl") {
      return chrome.storage.local.get({
        video_list: []
      }, function(data) {
        console.log(data);
        data.video_list.push(info.linkUrl);
        return chrome.storage.local.set({
          video_list: data.video_list
        });
      });
    }
  });

  chrome.storage.local.get({
    video_list: []
  }, function(data) {
    return console.log(data.video_list);
  });

}).call(this);
