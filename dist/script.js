(function() {
  var ControllerView, UrlListView, UrlView, Video, VideoList, VideoListView, VideoView, controller, urllistview, videolist, videolistview,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  Video = (function(superClass) {
    extend(Video, superClass);

    function Video() {
      return Video.__super__.constructor.apply(this, arguments);
    }

    return Video;

  })(Backbone.Model);

  VideoList = (function(superClass) {
    extend(VideoList, superClass);

    function VideoList() {
      this.save_storage = bind(this.save_storage, this);
      this.sync_storage = bind(this.sync_storage, this);
      return VideoList.__super__.constructor.apply(this, arguments);
    }

    VideoList.prototype.model = Video;

    VideoList.prototype.initialize = function() {
      return this.bind('remove', this.save_storage);
    };

    VideoList.prototype.sync_storage = function() {
      var dfd;
      this.reset();
      dfd = $.Deferred();
      console.log("Start sync");
      chrome.storage.local.get({
        video_list: []
      }, (function(_this) {
        return function(data) {
          var i, j, len, ref, url;
          ref = data.video_list;
          for (i = j = 0, len = ref.length; j < len; i = ++j) {
            url = ref[i];
            _this.add(new Video({
              url: url
            }));
          }
          console.log("Finish sync");
          return dfd.resolve();
        };
      })(this));
      return dfd.promise();
    };

    VideoList.prototype.save_storage = function() {
      var dfd, video_list;
      dfd = $.Deferred();
      console.log("Start save");
      video_list = _.map(this.models, function(model) {
        return model.get('url');
      });
      chrome.storage.local.set({
        video_list: video_list
      }, (function(_this) {
        return function(data) {
          console.log("Finish save");
          return dfd.resolve();
        };
      })(this));
      return dfd.promise();
    };

    return VideoList;

  })(Backbone.Collection);

  UrlView = (function(superClass) {
    extend(UrlView, superClass);

    function UrlView() {
      return UrlView.__super__.constructor.apply(this, arguments);
    }

    UrlView.prototype.tagName = "li";

    UrlView.prototype.initialize = function() {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'destroy', this.remove);
      return this.render();
    };

    UrlView.prototype.events = {
      'click .del': 'destroy'
    };

    UrlView.prototype.destroy = function() {
      return this.model.destroy();
    };

    UrlView.prototype.render = function() {
      $(this.el).html(_.template($('#tmplUrlView').html())(this.model.toJSON()));
      return this;
    };

    return UrlView;

  })(Backbone.View);

  UrlListView = (function(superClass) {
    extend(UrlListView, superClass);

    function UrlListView() {
      return UrlListView.__super__.constructor.apply(this, arguments);
    }

    UrlListView.prototype.el = '#urls';

    UrlListView.prototype.initialize = function() {
      return this.listenTo(this.collection, 'add', this.addChildView);
    };

    UrlListView.prototype.addChildView = function(video) {
      var urlview;
      urlview = new UrlView({
        model: video
      });
      return $(this.el).append(urlview.el);
    };

    return UrlListView;

  })(Backbone.View);

  VideoView = (function(superClass) {
    extend(VideoView, superClass);

    function VideoView() {
      return VideoView.__super__.constructor.apply(this, arguments);
    }

    VideoView.prototype.tagName = 'div';

    VideoView.prototype.className = 'video-container';

    VideoView.prototype.initialize = function() {
      $(this.el).css('z-index', $('.' + this.className).length + 1);
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'destroy', this.remove);
      return this.render();
    };

    VideoView.prototype.events = {
      'click .del': 'destroy',
      'click .reload': 'reload'
    };

    VideoView.prototype.destroy = function() {
      return this.model.destroy();
    };

    VideoView.prototype.reload = function() {
      return this.render();
    };

    VideoView.prototype.render = function() {
      var vid;
      $(this.el).html($('#tmplVideoView').html());
      vid = this.model.get('url').match(/video(\d+)/)[1];
      $('iframe', $(this.el)).attr('src', "http://flashservice.xvideos.com/embedframe/" + vid);
      return this;
    };

    return VideoView;

  })(Backbone.View);

  VideoListView = (function(superClass) {
    extend(VideoListView, superClass);

    function VideoListView() {
      return VideoListView.__super__.constructor.apply(this, arguments);
    }

    VideoListView.prototype.el = '#videos';

    VideoListView.prototype.initialize = function() {
      this.max_cols = 3;
      this.listenTo(this.collection, 'add', this.addChildView);
      return this.listenTo(this.collection, 'remove', this.resetPosition);
    };

    VideoListView.prototype.addChildView = function(video) {
      var videoview;
      videoview = new VideoView({
        model: video
      });
      $(this.el).append(videoview.el);
      return this.resetPosition();
    };

    VideoListView.prototype.columnUp = function() {
      this.max_cols++;
      return this.resetPosition();
    };

    VideoListView.prototype.columnDown = function() {
      if (--this.max_cols < 1) {
        this.max_cols = 1;
      }
      return this.resetPosition();
    };

    VideoListView.prototype.resetPosition = function() {
      var h, max_cols, w;
      max_cols = this.max_cols;
      w = $(window).width() / max_cols;
      h = w * 4 / 5;
      return $('.video-container', this.el).each(function(i) {
        $(this).css({
          width: w + "px",
          height: h + "px"
        });
        return $(this).animate({
          top: parseInt(i / max_cols) * h + "px",
          left: (i % max_cols) * w + "px"
        }, 'fast');
      });
    };

    return VideoListView;

  })(Backbone.View);

  ControllerView = (function(superClass) {
    extend(ControllerView, superClass);

    function ControllerView() {
      return ControllerView.__super__.constructor.apply(this, arguments);
    }

    ControllerView.prototype.el = '#controller';

    ControllerView.prototype.events = {
      'click .columnUp': 'columnUp',
      'click .columnDown': 'columnDown'
    };

    ControllerView.prototype.columnUp = function() {};

    ControllerView.prototype.columnDown = function() {};

    return ControllerView;

  })(Backbone.View);

  videolist = new VideoList();

  urllistview = new UrlListView({
    collection: videolist
  });

  videolistview = new VideoListView({
    collection: videolist
  });

  videolist.sync_storage();

  controller = new ControllerView();

  $('.columnUp').click(function() {
    return videolistview.columnUp();
  });

  $('.columnDown').click(function() {
    return videolistview.columnDown();
  });

  $(function() {
    return $('body').tooltip({
      selector: '[data-toggle="tooltip"]'
    });
  });

  chrome.storage.onChanged.addListener(function(changes, namespace) {
    var change;
    if (namespace !== 'local') {
      return false;
    }
    if (!(change = changes['video_list'])) {
      return false;
    }
    if (!(change.oldValue.length < change.newValue.length)) {
      return false;
    }
    console.log("Add Video");
    return videolist.add(new Video({
      url: change.newValue.pop()
    }));
  });

}).call(this);
