chrome.browserAction.onClicked.addListener (tab) ->
  chrome.tabs.create({url: chrome.extension.getURL('index.html')})

chrome.runtime.onInstalled.addListener ->
  chrome.contextMenus.create
    id: "dl"
    title: "mutlix"
    type: "normal"
    contexts: [ "link" ]

chrome.contextMenus.onClicked.addListener (info, tab) ->
  if info.menuItemId == "dl"

    chrome.storage.local.get {video_list:[]}, (data) ->
      console.log data
      data.video_list.push info.linkUrl
      chrome.storage.local.set {video_list: data.video_list}

chrome.storage.local.get {video_list: []}, (data) ->
  console.log data.video_list
