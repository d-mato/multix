class Video extends Backbone.Model

class VideoList extends Backbone.Collection
  model: Video
  initialize: ->
    @bind('remove', @save_storage)

  sync_storage: =>
    @reset()
    dfd = $.Deferred()
    console.log "Start sync"
    chrome.storage.local.get {video_list:[]}, (data) =>
      for url, i in data.video_list
        @add new Video({url:url})
      console.log "Finish sync"
      dfd.resolve()
    return dfd.promise()

  save_storage: =>
    dfd = $.Deferred()
    console.log "Start save"
    video_list = _.map @models, (model) ->
      model.get('url')
    chrome.storage.local.set {video_list: video_list}, (data) =>
      console.log "Finish save"
      dfd.resolve()
    return dfd.promise()

class UrlView extends Backbone.View
  tagName: "li"
  initialize: ->
    @listenTo(@model, 'change', @render)
    @listenTo(@model, 'destroy', @remove)
    @render()
  events: {
    'click .del': 'destroy'
  }
  destroy: ->
    @model.destroy()

  render: ->
    $(@el).html(_.template($('#tmplUrlView').html())(@model.toJSON()))
    @

class UrlListView extends Backbone.View
  el: '#urls'
  initialize: ->
    @listenTo(@collection, 'add', @addChildView)

  addChildView: (video) ->
    urlview = new UrlView({model: video})
    $(@el).append(urlview.el)

class VideoView extends Backbone.View
  tagName: 'div'
  className: 'video-container'
  initialize: ->
    $(@el).css('z-index', $('.'+@className).length+1)
    @listenTo(@model, 'change', @render)
    @listenTo(@model, 'destroy', @remove)
    @render()

  events: {
    'click .del': 'destroy'
    'click .reload': 'reload'
  }
  destroy: ->
    @model.destroy()
  reload: ->
    @render()

  render: ->
    $(@el).html($('#tmplVideoView').html())
    vid = @model.get('url').match(/video(\d+)/)[1]
    $('iframe',$(@el)).attr('src', "http://flashservice.xvideos.com/embedframe/"+vid)
    # $(@el).set_movability()
    @

class VideoListView extends Backbone.View
  el: '#videos'
  initialize: ->
    @max_cols = 3
    @listenTo(@collection, 'add', @addChildView)
    @listenTo(@collection, 'remove', @resetPosition)

  addChildView: (video) ->
    videoview = new VideoView({model: video})
    $(@el).append(videoview.el)
    @resetPosition()

  columnUp: ->
    @max_cols++
    @resetPosition()
  columnDown: ->
    @max_cols = 1 if --@max_cols < 1
    @resetPosition()

  resetPosition: ->
    max_cols = @max_cols
    w = $(window).width()/max_cols
    h = w*4/5
    $('.video-container', @el).each (i) ->
      $(@).css
        width: w + "px"
        height: h + "px"
      $(@).animate({
        top:  parseInt(i/max_cols)*h + "px"
        left: (i%max_cols)*w + "px"
      }, 'fast')

class ControllerView extends Backbone.View
  el: '#controller'
  events: {
    'click .columnUp': 'columnUp'
    'click .columnDown': 'columnDown'
  }
  columnUp: ->
  columnDown: ->

videolist = new VideoList()
urllistview = new UrlListView({collection: videolist})
videolistview = new VideoListView({collection: videolist})
videolist.sync_storage()

controller = new ControllerView()
$('.columnUp').click ->
  videolistview.columnUp()
$('.columnDown').click ->
  videolistview.columnDown()

# $('.reset-position').click(videolistview.resetPosition)
$ ->
  $('body').tooltip
    selector: '[data-toggle="tooltip"]'

chrome.storage.onChanged.addListener (changes, namespace) ->
  return false unless namespace == 'local'
  return false unless change = changes['video_list']
  return false unless change.oldValue.length < change.newValue.length
  console.log "Add Video"
  videolist.add new Video({url: change.newValue.pop()})


# movable ui
#$.fn.set_movability = ->
#  $('html,body').off 'click'
#  $('html,body').on 'click', (e) ->
#    $('.video-container').removeClass('movable')
#    $('#content').removeClass('moving')
#  
#  $(@).off 'mousedown'
#  $(@).on 'mousedown', (e) ->
#    $('#content').addClass('moving')
#    zi_border = parseInt($(@).css('z-index'))
#    $(@).siblings().each ->
#      zi = parseInt($(@).css('z-index'))
#      if zi > zi_border
#        $(@).css('z-index', zi-1)
#    $(@).addClass('movable')
#      .css('z-index',$('.video-container').length)
#      .attr('pos-base-y', e.pageY - $(@).position().top)
#      .attr('pos-base-x', e.pageX - $(@).position().left)
#        
#  $('html,body').off 'mousemove'
#  $('html,body').on 'mousemove', (e) ->
#    w = $('.movable')
#    w.css({
#      top:  e.pageY - w.attr('pos-base-y') + "px"
#      left: e.pageX - w.attr('pos-base-x') + "px"
#    })
#
